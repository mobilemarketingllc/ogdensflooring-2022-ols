<div class="d4p-about-minor">
    <h3><?php _e("Maintenance and Security Releases", "gd-security-headers"); ?></h3>
    <p>
        <strong><?php _e("Version", "gd-security-headers"); ?> <span>1.6</span></strong> &minus;
        Many CSP related improvements.
    </p>
    <p>
        <strong><?php _e("Version", "gd-security-headers"); ?> <span>1.5</span></strong> &minus;
        Many Feature Policy improvements.
    </p>
    <p>
        <strong><?php _e("Version", "gd-security-headers"); ?> <span>1.4</span></strong> &minus;
        Many CSP and Feature Policy improvements.
    </p>
    <p>
        <strong><?php _e("Version", "gd-security-headers"); ?> <span>1.3</span></strong> &minus;
        CSP and Feature Policy improvements.
    </p>
    <p>
        <strong><?php _e("Version", "gd-security-headers"); ?> <span>1.3</span></strong> &minus;
        Various updates and fixes.
    </p>
    <p>
        <strong><?php _e("Version", "gd-security-headers"); ?> <span>1.2</span></strong> &minus;
        Feature Policy Header. More CSP predefine rules. Various updates.
    </p>
    <p>
        <strong><?php _e("Version", "gd-security-headers"); ?> <span>1.1.1</span></strong> &minus;
        Bug fixes.
    </p>
    <p>
        <strong><?php _e("Version", "gd-security-headers"); ?> <span>1.1</span></strong> &minus;
        Panel with generated security headers. Various updates and improvements.
    </p>
    <p>
        <?php printf(__("For more information, see <a href='%s'>the changelog</a>.", "gd-security-headers"), 'admin.php?page=gd-security-headers-about&panel=changelog'); ?>
    </p>
</div>