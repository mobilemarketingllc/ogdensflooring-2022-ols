=== Grand Child Theme ===
Contributors: devteam
Tags: product listing, theme customization
Requires at least: 4.6
Tested up to: 5.0
Stable tag: 1.7.29
Requires PHP: 5.2.4
License: GPLv2 or later
License URI: https://www.gnu.org/licenses/gpl-2.0.html

A WordPress Grandchild Theme (as a plugin).

== Description ==

This is the long description.  No limit, and you can use Markdown (as well as in the following sections).

A WordPress Grandchild Theme (as a plugin)

== Installation ==

This section describes how to install the plugin and get it working.

e.g.

1. Upload the plugin files to the `/wp-content/plugins/plugin-name` directory, or install the plugin through the WordPress plugins screen directly.
1. Activate the plugin through the 'Plugins' screen in WordPress
1. Use the Settings->Plugin Name screen to configure the plugin
1. (Make your instructions match the desired user flow for activating and installing your plugin. Include any steps that might be needed for explanatory purposes)


== Frequently Asked Questions ==

= A question that someone might have =

An answer to that question.

= What about Grand Child Plugin? =

A WordPress Grandchild Theme (as a plugin)



== Changelog ==
1.7.29 =

* Audioeye integration

1.7.28 =

* audioeye integration and removal reviews from jsonld

1.7.27 =

* Birdeye css/js integration asscent color

1.7.26 =

* Birdeye css/js integration

1.7.25 =

* Birdeye review documentation

1.7.24 =

* Birdeye review integration

1.7.23 =

* 404 redirect hook changes

1.7.22 =

* Sku redirect hook

1.7.21 =

* No sale changes

1.7.20 =

* Floorte template heading change

1.7.19 =

* Floorte temp heading change

1.7.18 =

* Floorte temp link change

1.7.17 =

* Resolved warning and notices

1.7.16 =

* New Floorte Template Integration

1.7.15 =

* PHP 8 compatibility

1.7.14 =

* sale empty slide issue

1.7.13 =

* sale slider changes

1.7.12 =

* featured product slider room image change

1.7.11 =

* Sale priority changes

1.7.10 =

* Gravity form required field heading changes

1.7.9 =

* Sale Slider changes

1.7.8 =

*  Podium script change, product attribute, dirlink shortcode integration

1.7.7 =

*  Removal of write log

1.7.6 =

*  Blog sync, Floorte Slider, Popup image issue

1.7.5 =

*  Surface Applicability roomovo data feed integration.

1.7.4 =

*  New roomvo template changes.

1.7.3 =

*  New roomvo template integration.

1.7.2 =

*  Mohawk logo chnage and coretec colorwall removal

1.7.1 =

*  sale subheading Shortcode documentation and code changes

1.6.99 =

*  Shortcode documentation and code changes

1.6.98 =

*  Shortcode documentation

1.6.97 =

*  filters for shordcode work in seo title and descriptions

1.6.96 =

*  Retailer Seo location shortcode

1.6.95 =

*  Compare products script changes

1.6.94 =

*  Structured data changes

1.6.93 =

*  Blog post author and comment issue

1.6.92 =

*  Swell chat and review integrations

1.6.91 =

*  Custom alert banner condition changes

1.6.90 =

*  PDP zoom change, chat code changes

1.6.89 =

*  Location Country issue changes

1.6.88 =

*  Blank Slide issue

1.6.87 =

*  Swell integration changes

1.6.86 =

*  Seo details sync and product attributes changes

1.6.85 =

*  accessibility integration

1.6.84 =

*  Collection field changes and GTM tag function changes

1.6.83 =

*  Fixedwidth Banner code changes

1.6.82 =

*  Fullwidth Banner code changes

1.6.81 =

*  Beautifall sale redirect condition

1.6.80 =

*  Beautifall sale redirect changes

1.6.79 =

*  Beautifall sale settings and redirect

1.6.78 =

*  PLP product name issue

1.6.77 =

*  Swatch images issue

1.6.76 =

*  Chatmeter script value changes

1.6.75 =

*  Added light speed cache clearing cron job for every 6 hours.

1.6.74 =

*  Product json API sync changes.

1.6.73 =

*  Only shaw catalog sync and blog post sync changes.

1.6.72 =

*  Beautifall sale financing settings.

1.6.71 =

*  Beautifall sale settings.

1.6.70 =

*  Beautifall sale button settings.

1.6.69 =

*  Beautifall sale integration.

1.6.68 =

*  blog post insert integration changes

1.6.67 =

*  blog post sync integration changes

1.6.66 =

*  blog post delete integration changes

1.6.65 =

* Get coupon and blog post delete integration

1.6.64 =

* Coretec Colorwall - Choose style and choose color redirect for vinyl

1.6.63 =

* Coretec Colorwall - Choose style and choose color redirect

1.6.62 =

* Collection field changes due to coretec colorwall

1.6.61 =

* Store Hours and Holiday Custom banner changes

1.6.60 =

* Sale api changes as per api response

1.6.59 =

* Facebook domain verification 1 hour sync integration

1.6.58 =

* Facebook domain verification

1.6.57 =

* CSS changes for PDP and Slider browser issues

1.6.56 =

* Social icon changes.

1.6.55 =

* Post thumbnail alt tag,Removed exta sync for LVT, single area rug post type template.

1.6.54 =

* Removed exta sync for LVT.

1.6.53 =

* Removed exta sync for LVT.

1.6.52 =

* Removed exta sync for LVT.

1.6.51 =

* Js changes for facet on ipad.

1.6.50 =

* Blog Sync changes.

1.6.49 =

* LVT coretec sync changes.

1.6.48 =

* LVT coretec sync changes, plp changes and Robin brand logo.

1.6.47 =

* LVT coretec sync changes.

1.6.46 =

* COretec colorwall menu condition changes

1.6.45 =

* SEOURL constant change

1.6.44 =

* Client code label changes

1.6.43 =

* Dropped product endpoint flag

1.6.42 =

* Dropped product cron job integration

1.6.41 =

* Product sync changes

1.6.40 =

* database collection query change in PLP 

1.6.39 =

* database query change in PLP 

1.6.38 =

* Saturday Sync changes 

1.6.37 =

* JS changes for NEW PDP

1.6.36 =

* Sync Process change

1.6.35 =

* New PDP integration with meta title shortcode

1.6.34 =

* New PDP integration - Content 6 layout

1.6.33 =

* New PDP integration - Hardwood catalog 

1.6.32 =

* New PDP integration - Images for see in room button

1.6.31 =

* New PDP integration - Floorvana button

1.6.30 =

* New PDP integration and seo titles integration

1.6.29 =

* Changes for 404 status code

1.6.28 =

* Changes for 404 redirection code

1.6.28 =

* Removed compare product function

1.6.26 =

* Removed changes for Special cron added for laminate, hardwood, lvt

1.6.25 =

* changes for Special cron added for laminate, hardwood, lvt .

1.6.24 =

* changes for Special cron added for laminate, hardwood, lvt .

1.6.23 =

* Special cron added for laminate, hardwood, lvt.

1.6.22 =

* group by ondition for blog post and footer css changes

1.6.21 =

* group by ondition for blog post and footer css changes

1.6.20 =

* Removed query for extra fields from product meta data

1.6.19 =

* Removed extra fields from product meta data

1.6.18 =

* Disable product auto sync

1.6.17 =

* Thank you page integraton phase 3

1.6.16 =

* Thank you page integraton phase 2

1.6.15 =

* Thank you page integraton phase 1

1.6.14 =

* Roomvo lower case change for manufactuer in csv data.

1.6.13 =

* Roomvo lower case change for manufactuer.

1.6.12 =

* fl builder template issue resolved.

1.6.11 =

* 301 redirection changes.

1.6.10 =

* New Plp layout db query changes.

1.6.9 =

* New Plp layout change, Masland- Dixie home change, roomvo script changes.

1.6.8 =

* Product Limit change in New PLP and Dream weaver collection from design field.

1.6.7 =

* New plp facet collection and per page changes.

1.6.6 =

* New plp design changes and database query optimisaton.

1.6.5 =

* Masland Dixie changes for collection-design field.

1.6.4 =

* New PLP changes query table prefix.

1.6.3 =

* New PLP changes query optimisation.

1.6.2 =

* New PLP changes for fl-builder issue.

1.6.1 =

* New PLP, Cookies message GDPR, Masland-Dixie sync.

1.5.36 =

* Added cron job for sunday hardwood sync with 9.30 time.

1.5.35 =

* Added cron job for sunday hardwood sync.

1.5.34 =

* Added css and js for featured product slider arrow issue.

1.5.33 =

* Added css and js for featured product slider.

1.5.32 =

* Added compare site product with dataendpoint to delete with 301 redirect.

1.5.31 =

* Added ob_flush for roomvo csv integration.

1.5.30 =

* Changes for coretec colorwall pdf link.

1.5.29 =

* Changes roomvo csv for shaw manufatuer uses Brand.

1.5.28 =

* Changes roomvo csv for shaw manufatuer.

1.5.27 =

* Added post_exceprt in post sync blogpost.

1.5.26 =

* Changed condition for check post exist for Light & Airy blogpost.

1.5.25 =

* Changed condition for check post exist in blog sync.

1.5.24 =

* Removed Sun-Sational Savings Sale	duplicate post.

1.5.23 =

* Multisale integration Get Coupon changes as per brand in CDE.

1.5.22 =

* Multisale integration Get Coupon changes.

1.5.21 =

* Multisale integration Priority changes.

1.5.20 =

* Multisale integration.

1.5.19 =

* Changed cron job name for 301 redirect dropped.

1.5.18 =

* Added 301 cron jobby using dropped API category url.

1.5.17 =

* Added 301 cron jobby using dropped API category.

1.5.16 =

* Grab and Go sale integration for confirmation thank you.

1.5.15 =

* Grab and Go sale integration for loaction field type only store.

1.5.14 =

* Grab and Go sale integration for loaction field.

1.5.13 =

* Grab and Go sale integration.

1.5.12 =

* Bruce brand condition for default PDP template.

1.5.11 =

* Rugshop affilate link change as per CDE destination link.

1.5.10 =

* Rugshop affilate link change as per country for full width banner.

1.5.9 =

* Rugshop affilate link change as per country and Bruce brand Roomvo condition.

1.5.8 =

* Rugshop affilate link change as per country.

1.5.7 =

* Roomvo integration and CDE switch cron job.

1.5.6 =

* Roomvo integration and CDE switch.

1.5.5 =

* resolved banner image issue in layout 4

1.5.3 =

* Removed sync hardwood daily cron job

1.3.100 =

* Removed scheduled post delete code.

1.3.99 =

* Resolved update issue of Covid 19

1.3.98 =

* Updated content for Covid Alert page.

1.3.96 =

* Resolved alert draft issue.

1.3.95 =

* Added css for covid banner.

1.3.93 =

* Imported covid 19 template and banner with Alert page.

 1.3.92 =

* Imported covid 19 template and banner.

 1.3.91 =

* Added Condition for 404 redirect before adding 301 checkng if exist

 1.3.90 =

* Added todays cron for hardwood products.

 1.3.89 =

* Removed delete condition due to missing swatch column

 1.3.88 =

* Added c_limit prameter to cloudinary fetch urls.

 1.3.87 =

* Removed duplicate entries from 301 redirect.

 1.3.86 =

* Changed Condition for liqifire featured product loop.

 1.3.85 =

* Replaced cloudinary url to liquifire for slider.

 1.3.84 =

* Added cloudinary url to replace liquifire. 

 1.3.83 =

* CSV import and blog sync integrated.

 1.3.82 =

* Added condition for empty shapeand installation in pdp page.

 1.3.81 =

* Added condition for empty gallery image in pdp page.

 1.3.80 =

* Added condition for empty attribute in pdp page.

 1.3.79 =

* Created new shortcode for link in blog post.

 1.3.78 =

* Changed condition for Blog Sync as CDE date.

 1.3.77 =

* Changed condition for Blog Sync

= 1.3.76 =

* Optimized PDP page database query for speed performance.

= 1.3.74 =

* Changed condition for sale form hidden fields.

= 1.3.72 =

* Removed 301 same redirect url from redirection table.

= 1.3.70 =

* Created shortcode for financing which reurns only link.

= 1.3.69 =

* Changed condition for financing shortcode if empty links in CDE

= 1.3.68 =

* Changed condition for financing shortcode and  also changed get coupon on featured product slider

= 1.3.67 =

* Added slash to end of bredcrumbs url.

= 1.3.66 =

* Changed filter for robots txt.

= 1.3.65 =

* Added al tag for modal in post grid.

= 1.3.64 =

* Changed timing for 301 and 404 cron job.

= 1.3.63 =

* Added new rirection plugin logic and changed sync 301 redirect logic.

= 1.3.61 =

* Added css for comments off and changed sync cron job for other brand than shaw,mohawk.

= 1.3.59 =

* Added minified css and js

= 1.3.58 =

* Change condition for seo sitemap url.

= 1.3.57 =

* Change rugshop condition for gravity form hidden field.

= 1.3.56 =

* Added print coupon image functionality.

= 1.3.55 =

* Updated bump request function.

= 1.3.53 =

* Get Financing button text and url change on PLP

= 1.3.52 =

* Changed auto sync timetable for carpet shaw and mohawk brannd

= 1.3.51 =

* Get Financing button text and url change on PDP and PLP.

= 1.3.50 =

* H1 added for choose your color and choose your style.

= 1.3.48 =

* Changed fields for block country and added ip for whitelist.

= 1.3.47 =

* changes sale form hidden fields as cde response changed.

= 1.3.46 =

* Updated time for custom redirect cron.

= 1.3.44 =

* Updated block country options.

= 1.3.42 =

* Updated block country options.

= 1.3.41 =

* Updated cron timing for catalog.

= 1.3.40 =

* Updated wordpress timezone to UTC-5.

= 1.3.39 =

* Changed accessible code due to changed response in cde

= 1.3.38 =

* Changed interval time for laminate

= 1.3.37 =

* Changed interval time for laminate

= 1.3.36 =

* Clodinary and Daltile condition changed

= 1.3.34 =

* 301 url slashes added.

= 1.3.33 =

* Post Grid gallery image path changed.

= 1.3.32 =

* 301 custom redirect loop for option value changed.

= 1.3.31 =

* 301 custom redirect functionality changed.

= 1.3.30 =

* Wp_sfn_sync table condition and sync changed

= 1.3.29 =

* Username and password changes.

= 1.3.28 =

* Auto sync cron integration with Cloudinary image integration.

= 1.3.27 =

* Sale slide Response changes so Added condition for site_code 

= 1.3.26 =

* Changes for Chatmeter, Wells Fargo Link, Synchrony Link shortcodes

= 1.3.25 =
* change for new session script.
* Removed traffic source js.
* Added mmsession js at body end.

== Arbitrary section ==

You may provide arbitrary sections, in the same format as the ones above.  This may be of use for extremely complicated
plugins where more information needs to be conveyed that doesn't fit into the categories of "description" or
"installation."  Arbitrary sections will be shown below the built-in sections outlined above.

== A brief Markdown Example ==

Ordered list:

1. Some feature
1. Another feature
1. Something else about the plugin

Unordered list:

* something
* something else
* third thing

Here's a link to [WordPress](http://wordpress.org/ "Your favorite software") and one to [Markdown's Syntax Documentation][markdown syntax].
Titles are optional, naturally.

[markdown syntax]: http://daringfireball.net/projects/markdown/syntax
            "Markdown is what the parser uses to process much of the readme file"

Markdown uses email style notation for blockquotes and I've been told:
> Asterisks for *emphasis*. Double it up  for **strong**.

`<?php code(); // goes in backticks ?>`