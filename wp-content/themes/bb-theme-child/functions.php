<?php

// Defines
define( 'FL_CHILD_THEME_DIR', get_stylesheet_directory() );
define( 'FL_CHILD_THEME_URL', get_stylesheet_directory_uri() );

remove_action('wp_head', 'wp_generator');
// Classes
require_once 'classes/class-fl-child-theme.php';

// Actions
add_action( 'wp_enqueue_scripts', 'FLChildTheme::enqueue_scripts', 1000 );

add_action( 'wp_enqueue_scripts', function(){
    wp_enqueue_script("slick",get_stylesheet_directory_uri()."/resources/slick/slick.min.js","","",1);
    wp_enqueue_script("cookie",get_stylesheet_directory_uri()."/resources/jquery.cookie.min.js","","",1);
    wp_enqueue_script("child-script",get_stylesheet_directory_uri()."/script.js","","",1);
    
});

// Register menus
function register_my_menus() {
    register_nav_menus(
        array(
            'footer-1' => __( 'Footer Menu 1' ),
            'footer-2' => __( 'Footer Menu 2' ),
            'footer-3' => __( 'Footer Menu 3' ),
            'footer-4' => __( 'Footer Menu 4' ),
            'footer-5' => __( 'Footer Menu 5' ),
            'site-map' => __( 'Site Map' ),
        )
    );
}
add_action( 'init', 'register_my_menus' );


//add method to register event to WordPress init

add_action( 'init', 'register_daily_mysql_bin_log_event');
 
function register_daily_mysql_bin_log_event() {
    // make sure this event is not scheduled
    if( !wp_next_scheduled( 'mysql_bin_log_job' ) ) {
        // schedule an event
        wp_schedule_event( time(), 'daily', 'mysql_bin_log_job' );
    }
}

add_action( 'mysql_bin_log_job', 'mysql_bin_log_job_function' );
 
// Enable shortcodes in text widgets
add_filter('widget_text','do_shortcode');

//Facet Title Hook
add_filter( 'facetwp_shortcode_html', function( $output, $atts ) {
    if ( isset( $atts['facet'] ) ) {       
        $output= '<div class="facet-wrap"><strong>'.$atts['title'].'</strong>'. $output .'</div>';
    }
    return $output;
}, 10, 2 );

// Move Yoast to bottom
function yoasttobottom()
{
    return 'low';
}

add_filter('wpseo_metabox_prio', 'yoasttobottom');




// IP location FUnctionality
if (!wp_next_scheduled('cde_preferred_location_cronjob')) {
    
    wp_schedule_event( time() +  17800, 'daily', 'cde_preferred_location_cronjob');
}

add_action( 'cde_preferred_location_cronjob', 'cde_preferred_location' );

function cde_preferred_location(){

          global $wpdb;

          if ( ! function_exists( 'post_exists' ) ) {
              require_once( ABSPATH . 'wp-admin/includes/post.php' );
          }

          //CALL Authentication API:
          $apiObj = new APICaller;
          $inputs = array('grant_type'=>'client_credentials','client_id'=>get_option('CLIENT_CODE'),'client_secret'=>get_option('CLIENTSECRET'));
          $result = $apiObj->call(AUTHURL,"POST",$inputs,array(),AUTH_BASE_URL);


          if(isset($result['error'])){
              $msg =$result['error'];                
              $_SESSION['error'] = $msg;
              $_SESSION["error_desc"] =$result['error_description'];
              
          }
          else if(isset($result['access_token'])){

              //API Call for getting website INFO
              $inputs = array();
              $headers = array('authorization'=>"bearer ".$result['access_token']);
              $website = $apiObj->call(BASEURL.get_option('SITE_CODE'),"GET",$inputs,$headers);

             // write_log($website['result']['locations']);

              for($i=0;$i<count($website['result']['locations']);$i++){

                  if($website['result']['locations'][$i]['type'] == 'store'){

                      $location_name = isset($website['result']['locations'][$i]['city'])?$website['result']['locations'][$i]['name']:"";

                      $found_post = post_exists($location_name,'','','store-locations');

                          if( $found_post == 0 ){

                                  $array = array(
                                      'post_title' => $location_name,
                                      'post_type' => 'store-locations',
                                      'post_content'  => "",
                                      'post_status'   => 'publish',
                                      'post_author'   => 0,
                                  );
                                  $post_id = wp_insert_post( $array );

                                //  write_log( $location_name.'---'.$post_id);
                                  
                                  update_post_meta($post_id, 'address', $website['result']['locations'][$i]['address']); 
                                  update_post_meta($post_id, 'city', $website['result']['locations'][$i]['city']); 
                                  update_post_meta($post_id, 'state', $website['result']['locations'][$i]['state']); 
                                  update_post_meta($post_id, 'country', $website['result']['locations'][$i]['country']); 
                                  update_post_meta($post_id, 'postal_code', $website['result']['locations'][$i]['postalCode']); 
                                  if($website['result']['locations'][$i]['forwardingPhone']==''){

                                    update_post_meta($post_id, 'phone', $website['result']['locations'][$i]['phone']);  
                                    
                                  }else{

                                    update_post_meta($post_id, 'phone', $website['result']['locations'][$i]['forwardingPhone']);  
                                  }
                                                                     
                                  update_post_meta($post_id, 'latitude', $website['result']['locations'][$i]['lat']); 
                                  update_post_meta($post_id, 'longitue', $website['result']['locations'][$i]['lng']); 
                                  update_post_meta($post_id, 'monday', $website['result']['locations'][$i]['monday']); 
                                  update_post_meta($post_id, 'tuesday', $website['result']['locations'][$i]['tuesday']); 
                                  update_post_meta($post_id, 'wednesday', $website['result']['locations'][$i]['wednesday']); 
                                  update_post_meta($post_id, 'thursday', $website['result']['locations'][$i]['thursday']); 
                                  update_post_meta($post_id, 'friday', $website['result']['locations'][$i]['friday']); 
                                  update_post_meta($post_id, 'saturday', $website['result']['locations'][$i]['saturday']); 
                                  update_post_meta($post_id, 'sunday', $website['result']['locations'][$i]['sunday']); 
                                  
                          }else{

                                    update_post_meta($found_post, 'address', $website['result']['locations'][$i]['address']); 
                                    update_post_meta($found_post, 'city', $website['result']['locations'][$i]['city']); 
                                    update_post_meta($found_post, 'state', $website['result']['locations'][$i]['state']); 
                                    update_post_meta($found_post, 'country', $website['result']['locations'][$i]['country']); 
                                    update_post_meta($found_post, 'postal_code', $website['result']['locations'][$i]['postalCode']); 
                                    if($website['result']['locations'][$i]['forwardingPhone']==''){

                                    update_post_meta($found_post, 'phone', $website['result']['locations'][$i]['phone']);  
                                    
                                    }else{

                                    update_post_meta($found_post, 'phone', $website['result']['locations'][$i]['forwardingPhone']);  
                                    }
                                                                    
                                    update_post_meta($found_post, 'latitude', $website['result']['locations'][$i]['lat']); 
                                    update_post_meta($found_post, 'longitue', $website['result']['locations'][$i]['lng']); 
                                    update_post_meta($found_post, 'monday', $website['result']['locations'][$i]['monday']); 
                                    update_post_meta($found_post, 'tuesday', $website['result']['locations'][$i]['tuesday']); 
                                    update_post_meta($found_post, 'wednesday', $website['result']['locations'][$i]['wednesday']); 
                                    update_post_meta($found_post, 'thursday', $website['result']['locations'][$i]['thursday']); 
                                    update_post_meta($found_post, 'friday', $website['result']['locations'][$i]['friday']); 
                                    update_post_meta($found_post, 'saturday', $website['result']['locations'][$i]['saturday']); 
                                    update_post_meta($found_post, 'sunday', $website['result']['locations'][$i]['sunday']); 

                          }

                  }
              
              }

          }    

}





//get ipaddress of visitor

function getUserIpAddr() {
  $ipaddress = '';
  if (isset($_SERVER['HTTP_CLIENT_IP']))
      $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
  else if(isset($_SERVER['HTTP_X_FORWARDED_FOR']))
      $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
  else if(isset($_SERVER['HTTP_X_FORWARDED']))
      $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
  else if(isset($_SERVER['HTTP_X_CLUSTER_CLIENT_IP']))
      $ipaddress = $_SERVER['HTTP_X_CLUSTER_CLIENT_IP'];
  else if(isset($_SERVER['HTTP_FORWARDED_FOR']))
      $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
  else if(isset($_SERVER['HTTP_FORWARDED']))
      $ipaddress = $_SERVER['HTTP_FORWARDED'];
  else if(isset($_SERVER['REMOTE_ADDR']))
      $ipaddress = $_SERVER['REMOTE_ADDR'];
  else
      $ipaddress = 'UNKNOWN';
  
  if ( strstr($ipaddress, ',') ) {
          $tmp = explode(',', $ipaddress,2);
          $ipaddress = trim($tmp[1]);
  }
  return $ipaddress;
}


// Custom function for lat and long

function get_storelisting() {
  
  global $wpdb;
  $content="";
  
  $urllog = 'https://global-ds.cloud.netacuity.com/webservice/query';

  $response = wp_remote_post( $urllog, array(
      'method' => 'GET',
      'timeout' => 45,
      'redirection' => 5,
      'httpversion' => '1.0',
      'headers' => array('Content-Type' => 'application/json'),         
      'body' =>  array('u'=> 'b25d7667-74cc-4fcc-9adf-7b5f4f8f5bd0','ip'=> getUserIpAddr(),'dbs'=> 'all','trans_id'=> 'example','json'=> 'true' ),
      'blocking' => true,               
      'cookies' => array()
      )
  );
      
      $rawdata = json_decode($response['body'], true);
      $userdata = $rawdata['response'];

      $autolat = $userdata['pulseplus-latitude'];
      $autolng = $userdata['pulseplus-longitude'];
      if(isset($_COOKIE['preferred_store'])){

          $store_location = $_COOKIE['preferred_store'];
      }else{

          $store_location = '';
         
        }      
      
      $sql =  "SELECT post_lat.meta_value AS lat,post_lng.meta_value AS lng,posts.ID, 
              ( 3959 * acos( cos( radians(".$autolat." ) ) * cos( radians( post_lat.meta_value ) ) * 
              cos( radians( post_lng.meta_value ) - radians( ".$autolng." ) ) + sin( radians( ".$autolat." ) ) * 
              sin( radians( post_lat.meta_value ) ) ) ) AS distance FROM wp_posts AS posts
              INNER JOIN wp_postmeta AS post_lat ON post_lat.post_id = posts.ID AND post_lat.meta_key = 'latitude'
              INNER JOIN wp_postmeta AS post_lng ON post_lng.post_id = posts.ID AND post_lng.meta_key = 'longitue'  
              WHERE posts.post_type = 'store-locations' 
              AND posts.post_status = 'publish' GROUP BY posts.ID HAVING distance < 50000000000000000000 ORDER BY distance";

           

      $storeposts = $wpdb->get_results($sql);
      $storeposts_array = json_decode(json_encode($storeposts), true);      

     
      if($store_location ==''){
              
          $store_location = $storeposts_array['0']['ID'];
          $store_distance = round($storeposts_array['0']['distance'],1);
      }else{

      
          $key = array_search($store_location, array_column($storeposts_array, 'ID'));
          $store_distance = round($storeposts_array[$key]['distance'],1);           

      }
      
    
      $content = get_the_title($store_location); 
      $phone = get_field('phone', $store_location); 


  foreach ( $storeposts as $post ) {
$content_list .= '<div class=" store_wrapper" id ="'.$post->ID.'">
                
                    <h5 class="title-prefix">'.get_the_title($post->ID).' - '. round($post->distance,1).' MI</h5>
                    <h5 class="store-add"> '.get_field('address', $post->ID).'<br />'.get_field('city',$post->ID).', '.get_field('state',$post->ID).' '.get_field('postal_code', $post->ID).'</h5>';
                  

                    if($post->ID != '1128679'){
                       
                        if(get_field(strtolower(date("l")), $post->ID) == 'CLOSED'){

                            $content_list .= '<p class="store-hour">CLOSED TODAY</p>';   
    
                            }else{

                              $openuntil = explode("-",get_field(strtolower(date("l")), $post->ID));
    
                            $content_list .= '<p class="store-hour">OPEN UNTIL '.str_replace(' ', '', $openuntil[1]).'</p>';

    
                            }

                    }else{

                        $content_list .='<p class="store-hour"><span class="warehouse">Not a Showroom (Warehouse only) </p>';

                    }                  
                    $content_list .='<p class="store-phone"><a href="tel:'.get_field('phone', $post->ID).'">'.get_field('phone', $post->ID).'</a> </p>'; 
                    if($post->ID != '1143810'){
                        
                        $content_list .='<a href="javascript:void(0)" data-id="'.$post->ID.'" data-storename="'.get_the_title($post->ID).'" data-distance="'.round($post->distance,1).'" data-phone="'.get_field('phone', $post->ID).'" target="_self" class="store-cta-link choose_location">Choose Location</a>';
                        $content_list .='<a href="'.get_field('store_url',$post->ID).'" target="_self" class="store-cta-link view_location"> View Location</a>';
                    }
                   
                    $content_list .='</div>'; 
  } 
  

  $data = array();

  $data['header']= $content;
  $data['phone']= $phone;
  $data['list']= $content_list;

  echo json_encode($data);
      wp_die();
}


add_action( 'wp_ajax_nopriv_get_storelisting', 'get_storelisting', '1' );
add_action( 'wp_ajax_get_storelisting', 'get_storelisting','1' );



//choose this location FUnctionality

add_action( 'wp_ajax_nopriv_choose_location', 'choose_location' );
add_action( 'wp_ajax_choose_location', 'choose_location' );

function choose_location() {     
      
      $content = get_the_title($_POST['store_id']) ;      

      $content = '<div class="store_wrapper">';
      $content .='<div class="locationWrapFlyer"><div class="contentFlyer"><p>You&lsquo;re Shopping</p>
      <h3 class="header_location_name">'. get_the_title($_POST['store_id']) . '</h3>
      <div class="mobile"><a href="tel:'.$_POST['phone'].'">'.$_POST['phone'].'</a></div></div><div class="dropIcon"></div></div></div>';

      echo $content;

  wp_die();
}


add_filter( 'gform_pre_render_15', 'populate_product_location_form' );
add_filter( 'gform_pre_validation_15', 'populate_product_location_form' );
add_filter( 'gform_pre_submission_filter_15', 'populate_product_location_form' );
add_filter( 'gform_admin_pre_render_15', 'populate_product_location_form' );

add_filter( 'gform_pre_render_26', 'populate_product_location_form' );
add_filter( 'gform_pre_validation_26', 'populate_product_location_form' );
add_filter( 'gform_pre_submission_filter_26', 'populate_product_location_form' );
add_filter( 'gform_admin_pre_render_26', 'populate_product_location_form' );

add_filter( 'gform_pre_render_17', 'populate_product_location_form' );
add_filter( 'gform_pre_validation_17', 'populate_product_location_form' );
add_filter( 'gform_pre_submission_filter_17', 'populate_product_location_form' );
add_filter( 'gform_admin_pre_render_17', 'populate_product_location_form' );

add_filter( 'gform_pre_render_22', 'populate_product_location_form' );
add_filter( 'gform_pre_validation_22', 'populate_product_location_form' );
add_filter( 'gform_pre_submission_filter_22', 'populate_product_location_form' );
add_filter( 'gform_admin_pre_render_22', 'populate_product_location_form' );

add_filter( 'gform_pre_render_23', 'populate_product_location_form' );
add_filter( 'gform_pre_validation_23', 'populate_product_location_form' );
add_filter( 'gform_pre_submission_filter_23', 'populate_product_location_form' );
add_filter( 'gform_admin_pre_render_23', 'populate_product_location_form' );

add_filter( 'gform_pre_render_25', 'populate_product_location_form' );
add_filter( 'gform_pre_validation_25', 'populate_product_location_form' );
add_filter( 'gform_pre_submission_filter_25', 'populate_product_location_form' );
add_filter( 'gform_admin_pre_render_25', 'populate_product_location_form' );

add_filter( 'gform_pre_render_21', 'populate_product_location_form' );
add_filter( 'gform_pre_validation_21', 'populate_product_location_form' );
add_filter( 'gform_pre_submission_filter_21', 'populate_product_location_form' );
add_filter( 'gform_admin_pre_render_21', 'populate_product_location_form' );

function populate_product_location_form( $form ) {

foreach ( $form['fields'] as &$field ) {

    // Only populate field ID 12
    if ( $field->type != 'select' || strpos( $field->cssClass, 'populate-store' ) === false ) {
        continue;
    }      	

        $args = array(
            'post_type'      => 'store-locations',
            'posts_per_page' => -1,
            'post_status'    => 'publish'
        );										
      
         $locations =  get_posts( $args );

         $choices = array(); 

         foreach($locations as $location) {
            

             // $title =  get_post_meta( $location->ID, 'city', true ).', '.get_post_meta( $location->ID, 'state', true );

             $title = get_the_title($location->ID);
              
                   $choices[] = array( 'text' => $title, 'value' => $title );

          }
          wp_reset_postdata();

         // write_log($choices);

         // Set placeholder text for dropdown
         $field->placeholder = '-- Choose Location --';

         // Set choices from array of ACF values
         $field->choices = $choices;
    
}
return $form;
}

//custom schema add by basecamp request
add_action ( 'wp_head', 'hook_inHeader' );
function hook_inHeader() {

	$schema = '<script type="application/ld+json">
    [{"@context":"https://schema.org","@type":"HomeAndConstructionBusiness","name":"Ogden&#039;s Flooring","image":"https://ogdensflooring.com/wp-content/uploads/bb-p…ne-Bedroom-Wood-H_500x500-landscape-landscape.jpg","@id":"","url":"https://ogdensflooring.com/","telephone":"(385) 406-4563","address":{"@type":"PostalAddress","streetAddress":"852 EXPRESSWAY LN , UT","addressLocality":"SPANISH FORK","addressRegion":"UT","postalCode":"84660","addressCountry":"US"},"geo":{"@type":"GeoCoordinates","latitude":40.122415,"longitude":-111.6400508},"openingHoursSpecification":{"@type":"OpeningHoursSpecification","dayOfWeek":["Monday","Tuesday","Wednesday","Thursday","Friday"],"opens":"09:00","closes":"20:00"},"sameAs":["https://www.facebook.com/Ogdens.Flooring.Design/","https://twitter.com/ogdenflooring","https://www.instagram.com/ogdensflooring/"]},{"@context":"https://schema.org","@type":"HomeAndConstructionBusiness","name":"Ogden&#039;s Flooring","image":"https://ogdensflooring.com/wp-content/uploads/bb-p…ne-Bedroom-Wood-H_500x500-landscape-landscape.jpg","@id":"","url":"https://ogdensflooring.com/","telephone":"(435) 218-7351","address":{"@type":"PostalAddress","streetAddress":"2842 E 850 N","addressLocality":"SAINT GEORGE","addressRegion":"UT","postalCode":"84790","addressCountry":"US"},"geo":{"@type":"GeoCoordinates","latitude":37.1222994,"longitude":-113.5258364},"openingHoursSpecification":[{"@type":"OpeningHoursSpecification","dayOfWeek":["Monday","Tuesday","Wednesday","Thursday","Friday"],"opens":"09:00","closes":"18:00"},{"@type":"OpeningHoursSpecification","dayOfWeek":"Saturday","opens":"10:00","closes":"17:00"}],"sameAs":["https://www.facebook.com/Ogdens.Flooring.Design/","https://twitter.com/ogdenflooring","https://www.instagram.com/ogdensflooring/"]},{"@context":"https://schema.org","@type":"HomeAndConstructionBusiness","name":"Ogden&#039;s Flooring","image":"https://ogdensflooring.com/wp-content/uploads/bb-p…ne-Bedroom-Wood-H_500x500-landscape-landscape.jpg","@id":"","url":"https://ogdensflooring.com/","telephone":"(385) 412-6328","address":{"@type":"PostalAddress","streetAddress":"374 N STATE ST","addressLocality":"OREM","addressRegion":"UT","postalCode":"84057","addressCountry":"US"},"geo":{"@type":"GeoCoordinates","latitude":40.3039818,"longitude":-111.6972804},"openingHoursSpecification":[{"@type":"OpeningHoursSpecification","dayOfWeek":["Monday","Tuesday","Wednesday","Thursday","Friday"],"opens":"09:00","closes":"20:00"},{"@type":"OpeningHoursSpecification","dayOfWeek":"Saturday","opens":"09:00","closes":"18:00"}],"sameAs":["https://www.facebook.com/Ogdens.Flooring.Design/","https://twitter.com/ogdenflooring","https://www.instagram.com/ogdensflooring/"]},{"@context":"https://schema.org","@type":"HomeAndConstructionBusiness","name":"Ogden&#039;s Flooring","image":"https://ogdensflooring.com/wp-content/uploads/bb-p…ne-Bedroom-Wood-H_500x500-landscape-landscape.jpg","@id":"","url":"https://ogdensflooring.com/","telephone":"(385) 557-8009","address":{"@type":"PostalAddress","streetAddress":"783 E 12300 S STE B","addressLocality":"DRAPER","addressRegion":"UT","postalCode":"84020","addressCountry":"US"},"geo":{"@type":"GeoCoordinates","latitude":40.5272252,"longitude":-111.8691085},"openingHoursSpecification":[{"@type":"OpeningHoursSpecification","dayOfWeek":["Monday","Tuesday","Wednesday","Thursday","Friday"],"opens":"09:00","closes":"20:00"},{"@type":"OpeningHoursSpecification","dayOfWeek":"Saturday","opens":"09:00","closes":"18:00"}],"sameAs":["https://www.facebook.com/Ogdens.Flooring.Design/","https://twitter.com/ogdenflooring","https://www.instagram.com/ogdensflooring/"]},{"@context":"https://schema.org","@type":"HomeAndConstructionBusiness","name":"Ogden&#039;s Flooring","image":"https://ogdensflooring.com/wp-content/uploads/bb-p…ne-Bedroom-Wood-H_500x500-landscape-landscape.jpg","@id":"","url":"https://ogdensflooring.com/","telephone":"(385) 557-8012","address":{"@type":"PostalAddress","streetAddress":"8912 S STATE STREET","addressLocality":"SANDY","addressRegion":"UT","postalCode":"84070","addressCountry":"US"},"geo":{"@type":"GeoCoordinates","latitude":40.5897393,"longitude":-111.89164},"openingHoursSpecification":[{"@type":"OpeningHoursSpecification","dayOfWeek":["Monday","Tuesday","Wednesday","Thursday","Friday"],"opens":"09:00","closes":"20:00"},{"@type":"OpeningHoursSpecification","dayOfWeek":"Saturday","opens":"09:00","closes":"18:00"}],"sameAs":["https://www.facebook.com/Ogdens.Flooring.Design/","https://twitter.com/ogdenflooring","https://www.instagram.com/ogdensflooring/"]},{"@context":"https://schema.org","@type":"HomeAndConstructionBusiness","name":"Ogden&#039;s Flooring","image":"https://ogdensflooring.com/wp-content/uploads/bb-p…ne-Bedroom-Wood-H_500x500-landscape-landscape.jpg","@id":"","url":"https://ogdensflooring.com/","telephone":"(385) 247-0979","address":{"@type":"PostalAddress","streetAddress":"2464 W 12600 S #160","addressLocality":"RIVERTON","addressRegion":"UT","postalCode":"84065","addressCountry":"US"},"geo":{"@type":"GeoCoordinates","latitude":40.5230316,"longitude":-111.9527188},"openingHoursSpecification":[{"@type":"OpeningHoursSpecification","dayOfWeek":["Monday","Tuesday","Wednesday","Thursday","Friday"],"opens":"09:00","closes":"20:00"},{"@type":"OpeningHoursSpecification","dayOfWeek":"Saturday","opens":"09:00","closes":"18:00"}],"sameAs":["https://www.facebook.com/Ogdens.Flooring.Design/","https://twitter.com/ogdenflooring","https://www.instagram.com/ogdensflooring/"]},{"@context":"https://schema.org","@type":"HomeAndConstructionBusiness","name":"Ogden&#039;s Flooring","image":"https://ogdensflooring.com/wp-content/uploads/bb-p…ne-Bedroom-Wood-H_500x500-landscape-landscape.jpg","@id":"","url":"https://ogdensflooring.com/","telephone":"(385) 557-8021","address":{"@type":"PostalAddress","streetAddress":"2983 W 4700 S","addressLocality":"TAYLORSVILLE","addressRegion":"UT","postalCode":"84129","addressCountry":"US"},"geo":{"@type":"GeoCoordinates","latitude":40.6673107,"longitude":-111.9634741},"openingHoursSpecification":[{"@type":"OpeningHoursSpecification","dayOfWeek":["Monday","Tuesday","Wednesday","Thursday","Friday"],"opens":"09:00","closes":"20:00"},{"@type":"OpeningHoursSpecification","dayOfWeek":"Saturday","opens":"09:00","closes":"18:00"}],"sameAs":["https://www.facebook.com/Ogdens.Flooring.Design/","https://twitter.com/ogdenflooring","https://www.instagram.com/ogdensflooring/"]},{"@context":"https://schema.org","@type":"HomeAndConstructionBusiness","name":"Ogden&#039;s Flooring","image":"https://ogdensflooring.com/wp-content/uploads/bb-p…ne-Bedroom-Wood-H_500x500-landscape-landscape.jpg","@id":"","url":"https://ogdensflooring.com/","telephone":"(435) 315-0789","address":{"@type":"PostalAddress","streetAddress":"110 SOUTH MAIN STREET","addressLocality":"HEBER CITY","addressRegion":"UT","postalCode":"84032","addressCountry":"US"},"geo":{"@type":"GeoCoordinates","latitude":40.5061393,"longitude":-111.413755},"openingHoursSpecification":[{"@type":"OpeningHoursSpecification","dayOfWeek":["Monday","Tuesday","Wednesday","Thursday","Friday"],"opens":"09:00","closes":"18:00"},{"@type":"OpeningHoursSpecification","dayOfWeek":"Saturday","opens":"09:00","closes":"15:00"}],"sameAs":["https://www.facebook.com/Ogdens.Flooring.Design/","https://twitter.com/ogdenflooring","https://www.instagram.com/ogdensflooring/"]},{"@context":"https://schema.org","@type":"HomeAndConstructionBusiness","name":"Ogden&#039;s Flooring","image":"https://ogdensflooring.com/wp-content/uploads/bb-p…ne-Bedroom-Wood-H_500x500-landscape-landscape.jpg","@id":"","url":"https://ogdensflooring.com/","telephone":"(385) 442-4832","address":{"@type":"PostalAddress","streetAddress":"1878 W 5150 S","addressLocality":"ROY","addressRegion":"UT","postalCode":"84067","addressCountry":"US"},"geo":{"@type":"GeoCoordinates","latitude":41.1704789,"longitude":-112.0250013},"openingHoursSpecification":[{"@type":"OpeningHoursSpecification","dayOfWeek":["Monday","Tuesday","Wednesday","Saturday"],"opens":"09:00","closes":"18:00"},{"@type":"OpeningHoursSpecification","dayOfWeek":["Thursday","Friday"],"opens":"09:00","closes":"20:00"}],"sameAs":["https://www.facebook.com/Ogdens.Flooring.Design/","https://twitter.com/ogdenflooring","https://www.instagram.com/ogdensflooring/"]},{"@context":"https://schema.org","@type":"HomeAndConstructionBusiness","name":"Ogden&#039;s Flooring","image":"https://ogdensflooring.com/wp-content/uploads/bb-p…ne-Bedroom-Wood-H_500x500-landscape-landscape.jpg","@id":"","url":"https://ogdensflooring.com/","telephone":"(435) 216-7693","address":{"@type":"PostalAddress","streetAddress":"2232 NORTH MAIN STREET","addressLocality":"CEDAR CITY","addressRegion":"UT","postalCode":"84721","addressCountry":"US"},"geo":{"@type":"GeoCoordinates","latitude":37.7175671,"longitude":-113.0585803},"openingHoursSpecification":{"@type":"OpeningHoursSpecification","dayOfWeek":["Monday","Tuesday","Wednesday","Saturday","Thursday","Friday"],"opens":"09:00","closes":"18:00"},"sameAs":["https://www.facebook.com/Ogdens.Flooring.Design/","https://twitter.com/ogdenflooring","https://www.instagram.com/ogdensflooring/"]}]
    </script>';
	
		echo $schema;	
	
} ;